///////////////////////////////////////
// Beginning of the script
personnes["B12"] = ["Salle de TD"]
images["B12"] = [""]

var MAP_WIDTH  = 1400;
var MAP_HEIGHT = 1250;

var mapContainer = document.getElementById("map");
var map = new Raphael(mapContainer, MAP_WIDTH, MAP_HEIGHT);
var style = {
    fill: "#fff",
    stroke: "#aaa",
    "stroke-width": 1,
    "stroke-linejoin": "round",
    cursor: "pointer"
};

// Background image
var img = map.image("static/INFO/Plan_IUT_RDC.jpg", 0, 0, 1500, 1050);

var bureaux = {};
//bureaux["B14"] = map.path("m 639.8,480.6 0,311.6 236.7,-4.3 -1.7,-308.1 z");
bureaux["B1E"] = map.path("m 618.9,28.8 0,368.2 276.8,1.7 0,-371.6 z");
bureaux["B12"] = map.path("m 283.7,28.8 259.4,-0.8 2.6,269.8 -260.2,0 z");
bureaux["B11"] = map.path("m 283.7,298.7 262.8,0.8 -0.8,168.0 -260.2,0 z");
bureaux["B10"] = map.path("m 283.7,465.8 262.0,1.7 -0.8,87.9 -257.6,-0.8 -1.7,-0.8 -0.8,0.8 z");
bureaux["B9"]  = map.path("m 285.5,665.1 260.2,-0.8 0,126.2 -262.8,0 z");
bureaux["B8"]  = map.path("m 282.9,790.5 261.1,0.8 0.8,147.1 -255.9,-0.8 -4.3,-0.8 z");
bureaux["B7"]  = map.path("m 894.8,28.8 111.4,0 -1.7,224.5 -107.9,0 z");
bureaux["B6"]  = map.path("m 1006.2,27.9 222.8,0.8 -0.8,142.7 -219.3,0 z");
bureaux["B5"]  = map.path("m 1054.1,173.3 174.9,0 -2.6,121.8 -174.9,2.6 z");
bureaux["B4"]  = map.path("m 966.2,300.4 261.1,-1.7 -2.6,135.7 -257.6,0 z");
bureaux["B3"]  = map.path("m 970.4,434.5 c 254.1,-1.7 255.9,0.8 255.9,0.8 l -1.7,116.6 -258.5,-0.8 z");
bureaux["B2"]  = map.path("m 967.9,663.4 261.1,1.7 -0.8,137.5 -262.8,0 z");
bureaux["B1"]  = map.path("m 967.1,802.7 260.2,-0.8 1.741,138.4 -262.0,-2.6 z");

// For raphaeljs sets containing the office (path) plus the picture and text
var pix = {};

// Initialisation:
for(var bName in bureaux) {
    bureaux[bName].attr(style);
    //bureaux[bName].attr({href:bName});
    // Add office numbers
    var pt = bureaux[bName].getPointAtLength(bureaux[bName].getTotalLength());
    var text = map.text(pt.x + 35 , pt.y + 20, bName).attr({fill: "black",font: "20px sans-serif"});
    if (bName == "B1E") { // Cas du bureau 1er étage
	var text2 = map.text(pt.x + 110 , pt.y + 20, "(1er étage)").attr({fill: "black",font: "20px sans-serif"});
    }
    pix[bName] = map.set();
    pix[bName].push(bureaux[bName]);
    // Add people
    if (bName in personnes) { //in case there are less people than offices
        for(index = 0 ; index < personnes[bName].length ; index++)
        {
	    var img;
	    var txt;
	    if (bName != "B7") {
	        img = map.image(images[bName][index], pt.x + 10 + (110*index), pt.y + 30, 100, 100);
	        txt = map.text(pt.x + 60 + (110*index), pt.y + 150, personnes[bName][index]).attr({fill: "black",font: "10px sans-serif"});;
	    } else { // Cas du bureau B7 en orientation Nord-Sud
	        img = map.image(images[bName][index], pt.x + 10, pt.y + 30 + (140*index), 100, 100);
	        txt = map.text(pt.x + 60, pt.y + 150 + (140*index), personnes[bName][index]).attr({fill: "black",font: "10px sans-serif"});;
	    }
	    pix[bName].push(img,txt);
        }
    }
    // Hide offices
    pix[bName].attr({opacity : 0.1});
}

for(var bName in bureaux) {
    (function (buro, name) {
	buro.hover(
	    function() {
		buro.attr({ opacity : 1 });
                buro[0].attr({ fill :"#A8BED5" });
	    }, function() {
		buro.attr({ opacity : 0.1 });		
		buro[0].attr({ fill :"#FFFFFF" });		
	    });
    })(pix[bName],bName);
}

function clear () {
    for (var bName in bureaux) {
	pix[bName].attr({ opacity : 0.1 });		
	pix[bName][0].attr({ fill :"#FFFFFF" });	
    }
    return false;
}

// To move to the bureau when clicking on a bureau link in the people table:
people = document.getElementById("people");
people.onclick = function (e) {
    clear(pix)
    e = e || window.event;
    var target = e.target || e.srcElement || document;
    if (target.tagName == "A") {
        var txt = decodeURIComponent(target.href.substring(target.href.indexOf("#") + 1));
        document.getElementById("leplan").click();
        pix[txt].attr({ opacity : 1});
        pix[txt][0].attr({ fill :"#A8BED5" });
        return false;
    }
};
