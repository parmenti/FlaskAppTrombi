///////////////////////////////////////
// Beginning of the script
var MAP_WIDTH  = 1980;
var MAP_HEIGHT =  950;

var mapContainer = document.getElementById("map");
var map = new Raphael(mapContainer, MAP_WIDTH, MAP_HEIGHT);
var style = {
    fill: "#fff",
    stroke: "#aaa",
    "stroke-width": 1,
    "stroke-linejoin": "round",
    cursor: "pointer"
};

// Background image
var img = map.image("static/Administration/plan-adm2.png", 0, 0, 1980, 950);

var bureaux = {};
bureaux["Directeur"] = map.path("m 1640.0548,195.66814 1.7788,181.43774 240.1382,-1.7788 -3.5576,-177.88014 z");
bureaux["Secretariat"] = map.path("m 1700.534,5.3364039 0,184.9953361 179.659,3.5576 1.7788,-186.7741348 z");
bureaux["RH1"] = map.path("m 1522.6539,7.1152052 0,122.7372948 174.3225,-1.77881 3.5576,-126.2948887 z");
bureaux["RH2"] = map.path("m 1405.253,3.5576026 1.7788,129.8524974 115.6221,-1.7788 -1.7788,-129.8524987 z");
bureaux["Dir-adjoints"] = map.path("m 1229.1517,0 0,133.4101 176.1013,-1.7788 -1.7788,-128.0736974 z");
bureaux["RSA"] = map.path("m 1053.0504,3.5576026 0,126.2948974 172.5437,-1.77881 L 1227.3729,0 Z");
bureaux["Comm"] = map.path("m 933.87069,193.88934 0,183.21654 117.40091,-1.7788 1.7788,-183.21654 z");
bureaux["Finance"] = map.path("m 1051.2716,192.11054 0,184.99534 117.4009,0 1.7788,-184.99534 z");
bureaux["Finance1"] = map.path("m 1170.4513,190.33174 0,188.55294 176.1013,-1.7788 -3.5576,-183.21654 z");
bureaux["Service_Info"] = map.path("m 1650.7276,450.03673 5.3364,250.81098 234.8018,-1.7788 -1.7788,-247.25338 z");
bureaux["Accueil"] = map.path("m 1535.1055,451.81553 3.5576,122.73729 112.0645,0 0,-124.51609 z");
bureaux["Scolarite1"] = map.path("m 1245.1609,450.03673 0,129.8525 234.8018,-1.77881 -1.7788,-126.29489 z");
bureaux["Scolarite"] = map.path("m 1125.9812,453.59433 -1.7788,122.73729 117.4009,0 -1.7788,-124.51609 z");
bureaux["STechnique"] = map.path("m 832.47901,640.36847 0,183.21653 115.62209,1.77881 1.7788,-188.55294 z");
bureaux["Reprographie"] = map.path("m 1179.3453,638.58967 0,188.55294 c 0,0 240.1381,5.3364 240.1381,-1.7788 0,-7.11521 -5.3364,-186.77414 -5.3364,-186.77414 z");

// For raphaeljs sets containing the office (path) plus the picture and text
var pix = {};

// Initialisation:
for(var bName in bureaux) {
    bureaux[bName].attr(style);
    //bureaux[bName].attr({href:bName});
    // Add office numbers (not for Admin)
    var pt = bureaux[bName].getPointAtLength(bureaux[bName].getTotalLength());
    //var text = map.text(pt.x + 35 , pt.y + 20, bName).attr({fill: "black",font: "20px sans-serif"});
    
    pix[bName] = map.set();
    pix[bName].push(bureaux[bName]);
    // Add people
    if (bName in personnes) { //in case there are less people than offices
        for(index = 0 ; index < personnes[bName].length ; index++)
        {
	    var img;
	    var txt;
	    if (bName != "B7") { //orientation Ouest-Est (default)
	        img = map.image(images[bName][index], pt.x + 10 + (110*index), pt.y + 30, 100, 100);
	        txt = map.text(pt.x + 60 + (110*index), pt.y + 150, personnes[bName][index]).attr({fill: "black",font: "10px sans-serif"});;
	    } else { // orientation Nord-Sud (pour un pseudo bureau B7)
	        img = map.image(images[bName][index], pt.x + 10, pt.y + 30 + (140*index), 100, 100);
	        txt = map.text(pt.x + 60, pt.y + 150 + (140*index), personnes[bName][index]).attr({fill: "black",font: "10px sans-serif"});;
	    }
	    pix[bName].push(img,txt);
        }
    }
    // Hide offices
    pix[bName].attr({opacity : 0.1});
}

for(var bName in bureaux) {
    (function (buro, name) {
	buro.hover(
	    function() {
		buro.attr({ opacity : 1 });
                buro[0].attr({ fill :"#A8BED5" });
	    }, function() {
		buro.attr({ opacity : 0.1 });		
		buro[0].attr({ fill :"#FFFFFF" });
	    });
    })(pix[bName],bName);
}

function clear () {
    for (var bName in bureaux) {
	pix[bName].attr({ opacity : 0.1 });		
	pix[bName][0].attr({ fill :"#FFFFFF" });	
    }
    return false;
}

// To move to the bureau when clicking on a bureau link in the people table:
people = document.getElementById("people");
people.onclick = function (e) {
    clear(pix)
    e = e || window.event;
    var target = e.target || e.srcElement || document;
    if (target.tagName == "A") {
        var txt = decodeURIComponent(target.href.substring(target.href.indexOf("#") + 1));
        document.getElementById("leplan").click();
        pix[txt].attr({ opacity : 1});
        pix[txt][0].attr({ fill :"#A8BED5" });
        return false;
    }
};
