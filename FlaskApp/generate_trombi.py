#-*- coding: utf-8 -*-
#! /usr/bin/python3
#
# Script to get profile pictures from Harpege numbers
#
# Author: yannick.parmentier@univ-orleans.fr
# 2015/11/10

###import urllib2  ### python2 only
###import urllib.request
import requests #### for python3
import stat
import csv
import sys
import os
import os.path
import shutil
import getopt
import datetime
from PIL import Image, ImageFile
from fpdf import FPDF, HTMLMixin
from fpdf.html import hex2dec

class MyFPDF(FPDF, HTMLMixin):
    def header(this):
        # Logo
        this.image(os.path.dirname(os.path.realpath(__file__)) + '/static/logo-iuto.png',10,5,33)
        # Arial bold 15
        this.set_font('Arial','B',15)
        # Move to the right
        this.cell(80)
        # Line break
        this.ln(20)

    # Page footer
    def footer(this):
        # Position at 1.5 cm from bottom
        this.set_y(-15)
        # Arial italic 8
        this.set_font('Arial','I',8)
        # Page number
        this.cell(0,10,'Page '+str(this.page_no())+'/{nb}',0,0,'C')
        # date
        this.cell(0,10, str(datetime.date.today().year)+'/'+str(datetime.date.today().month)+'/'+str(datetime.date.today().day), 0, 0, 'R')
    
class ExcelFr(csv.excel):
    delimiter = ";"
 
csv.register_dialect('excel-fr', ExcelFr())

version = '1.0'
verbose = True

def usage():
    use = """
generate-trombi.py version %s

Usage: ls [OPTION] ... -f FILE -d DPT -m MAPFILE ...
Génère une liste des personnels pour le département DPT à partir de la liste contenue dans le fichier FILE (au format CSV).

Options:
  -m, --map MAPFILE          inclure le plan du département (si disponible)
  -l, --local                sauvergarder une copie locale des photos des personnels
  -p, --pdf                  générer un trombinoscope au format pdf
  -o, --outdir DIR           placer les fichiers générés dans le répertoire DIR (par défaut '.')
""" % version
    print(use)

def main(argv):
    filename = ''
    dptname  = ''
    mapfile  = ''
    withlocal= False
    withpdf  = False
    outdir   = '.'
    
    try:                                
        opts, args = getopt.getopt(argv, 'hf:d:m:lpo:', ['help', 'file=', 'departement=', 'map=', 'local', 'pdf', 'outdir']) 
    except getopt.GetoptError: 
        usage()    
        exit(2)  
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            exit(0)
        elif opt in ('-f', '--file'):
            filename = arg
        elif opt in ('-d', '--departement'):
            dptname  = arg
        elif opt in ('-m', '--map'):
            mapfile  = arg
        elif opt in ('-l', '--local'):
            withlocal= True
        elif opt in ('-p', '--pdf'):
            withpdf  = True #and pdfk #pdfkit no longer used
        elif opt in ('-o', '--outdir'):
            outdir   = arg

    #print(opts) ##debug only
            
    if filename =='':
        raise Exception('Argument manquant -f FILE (fichier contenant la liste des utilisateurs)')
        exit(1)

    if dptname == '':
        raise Exception('Argument manquant -d DPT (departement concerne)')
        exit(4)

    # Check if this is a CSV file and it is not corrupted (light check)
    fileOK = True
    csv_fileh = open(filename, 'rb')
    if filename[-4:] != '.csv':
        fileOK = False
    else:
        try:
            dialect = csv.Sniffer().sniff(csv_fileh.read(1024))
            # Don't forget to reset the read position back to the start of
            # the file before reading any entries.
            csv_fileh.seek(0)
        except csv.Error:
            # File appears not to be in CSV format; move along
            print('File appears not to be in CSV format.')
            fileOK = False
    if not fileOK :
        raise Exception('Format de fichier non-supporté (vérifier que c\'est un fichier cvs avec le ; comme séparateur)')
        exit(3)
    csv_fileh.close()
    
    # Eventually call the function
    if fileOK:
        res = generate(filename, dptname, mapfile, withlocal, withpdf, outdir)
        return res

    
#### BEGINNING OF THE PROGRAM
# filename : csv file contenant la liste des membres
# departement : nom du departement
# localpix : booleen indiquant s'il faut utiliser les photos
#            des cartes atout centre
#            ou celles de l'annuaire
#            POUR LA LISTE ET LE PLAN (pour trombi toujours local par defaut)
# pdf : boolean indiquant si on veut generer un pdf
def generate(filename, departement, mapfile='',localpix=False, pdf=False, outd = '.'):
    # mapi : booleen indiquant si une carte des bureaux est dispo
    mapi = True if mapfile != '' else False
    # for log:
    if verbose:
        print('Dpt: '+departement+'\nfichier: ' + filename + '\ncarte: '+ str(mapi) + '\nimages locales: ' + str(localpix) + '\ntrombi pdf: ' + str(pdf) + '\noutdir: ' + outd + '\n')

    outd += '/'
    staticrep = os.path.dirname(os.path.realpath(__file__)) + '/static/'
    #print(outd) #debug only
    #print(filename) #debug only
    #print(mapfile) #debug only
    ## Copy of the required files (style, maps)
    if not (os.path.isfile(outd + 'trombi.css')):
        shutil.copy(staticrep + 'trombi.css',outd + 'trombi.css')
    if mapi and not (os.path.isfile(outd + 'raphael-min.js')):
        shutil.copy(staticrep + 'raphael-min.js',outd + 'raphael-min.js')
    if mapfile != '' and not (os.path.isfile(outd + mapfile)):
        shutil.copy(staticrep + mapfile,outd + mapfile)
    if mapfile != '' and not (os.path.isfile(outd + 'templateMapINFO.js')):
        shutil.copy(staticrep + 'templateMapINFO.js',outd + 'templateMapINFO.js')
    if mapfile != '' and not (os.path.isfile(outd + 'templateMapAdministration.js')):
        shutil.copy(staticrep + 'templateMapAdministration.js',outd + 'templateMapAdministration.js')
        
    # Variables for configuration files
    imag = {}
    pers = {}

    datetrombi = str(datetime.date.today().day)+'/'+str(datetime.date.today().month)+'/'+str(datetime.date.today().year)
    
    menuplan = ''
    if mapi:
        menuplan = '        <li><a href="#plan" id="leplan">Plan des bureaux</a>'

    menupdf = ''
    if pdf:
        menupdf =  '        <li><a href="static/%s/%s-Trombi.pdf">Trombinoscope (pdf)</a>'%(departement, departement)

    htmlBegin = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Département %s - IUT d'Orléans</title>
        <link rel="stylesheet" type="text/css" href="static/%s/trombi.css">
        <link rel="stylesheet" type="text/css" href="static/bootstrap.min.css">
    </head>
    <body>
    """ % (departement, departement)

    htmlMenu = """
      <!-- Menu de navigation du site -->
      <ul class="navbar">
          <li><a href=\"/\">Retour</a>
          <li><a href="#pers">Personnels</a>
          %s
          %s
      </ul>
      <!-- Contenu principal -->
      <a name="top"></a>
      <h1>Équipe du Département %s - IUT d'Orléans au %s</h1>
      <br/><br/>
      <a name="pers"></a>
      <h2>Personnels</h2>
    """ % (menuplan,menupdf,departement,datetrombi)

    htmlEnd = ''
    if mapi:
        htmlEnd = """
        <br/><br/>
        Pour changer votre photo, merci de vous connecter au service dédié <a href="http://photo.univ-orleans.fr">ici</a> (lien interne à l'Université, accès local ou par VPN uniquement).
        <br/><br/>        <a href="#top">[Retour en haut de page]</a>    
        <br/><br/>
    
        <a name="plan"></a>
        <h2>Plan des bureaux</h2>
    
        <div id="map"></div>
        <script src="static/%s/raphael-min.js"></script>
        <script src="static/%s/%s-map.js"></script>

        <a href="#top">[Retour en haut de page]</a>
        <br/><br/>
      </body> 
      </html>
      """ % (departement, departement, departement)
    else:
        htmlEnd = """
        <br/><br/>
        Pour changer votre photo, merci de vous connecter au service dédié <a href="http://photo.univ-orleans.fr">ici</a> (lien interne à l'Université, accès local ou par VPN uniquement).
        <br/><br/>
        <a href="#top">[Retour en haut de page]</a>    
        <br/><br/>
      </body> 
      </html>
      """
    # Counter for number of people per row (trombi file)
    i = 0
    il= 0 #(idem for lines)

    # Open html output file
    htmlfile = open(outd + '%s.html'%departement,'w',encoding="utf-8") ##this one will serve for online checking

    # For pdf output file
    mypdf=MyFPDF('P','mm','A4')
    mypdf.set_auto_page_break(False)
    mypdf.alias_nb_pages()
    mypdf.add_page()
    mypdf.set_text_color(*hex2dec('#800080'))#('#A020F0'))
    
    # include the html template
    ##Main file:
    htmlfile.write(htmlBegin)
    htmlfile.write(htmlMenu)
    htmlfile.write('    <table border=0 class=\"table table-striped\" id=\"people\">\n')
    htmlfile.write('      <tr>\n')
    htmlfile.write('         <th id="nom"     class="col-xs-2" width=20%>Nom</th> \n')
    htmlfile.write('         <th id="prenom"  class="col-xs-1" width=15%>Prénom</th> \n')
    htmlfile.write('         <th id="resp"    class="col-xs-2" width=27%>Statut</th> \n')
    htmlfile.write('         <th id="matiere" class="col-xs-2" width=20%>Matière</th> \n')
    htmlfile.write('         <th id="mail"    class="col-xs-1" width=5%>Mail</th> \n')
    htmlfile.write('         <th id="poste"   class="col-xs-1" width=10%>Poste</th> \n')
    htmlfile.write('         <th id="bureau"  class="col-xs-1" width=3%>Bureau</th> \n')
    htmlfile.write('      </tr> \n')
    htmlfile.write('      <tbody> \n')
    htmlfile.write('        <tr>\n')

    ##Trombi file:
    to_del = [] #to remove local pix
    
    effective_page_width = mypdf.w - 2*mypdf.l_margin
    column_width  = 45
    column_spacing=  5
    mypdf.ln(-8)
    mypdf.cell(40)
    #mypdf.cell(115,15,unicode('Équipe du département %s - IUT d\'Orléans'%departement,'utf-8'), border=0)
    mypdf.cell(115,15,'Équipe du département %s - IUT d\'Orléans'%departement,'utf-8')
    mypdf.ln(10)
    mypdf.set_font('Arial','I',10)
    
    # process the input file
    file=open(filename,'r', encoding="utf-8")
    try:
        cr = csv.reader(file,'excel-fr')
        for row in cr:
            #print(' ') ##for debugging only
            if len(row)>0 and row[0][0] != '%':

                # Utilisation des photos de la carte atout centre pour trombi (defaut)
                atoutpix = True # to check whether the atout centre pix are available (for each picture)
                
                #0. Read input file
                harpege =row[0]#.decode('utf8')
                nom     =row[1].upper()#.decode('utf8').upper()
                prenom  =row[2]#.decode('utf8')
                domaine =row[3]#.decode('utf8')
                email   =row[4]#.decode('utf8')
                #email   =prenom.lower().replace(' ','')+'.'+nom.lower()+'@univ-orleans.fr'
                fonction = ''
                bureau   = ''
                tel      = ''
                if len(row)>5:
                    fonction=row[5]#.decode('utf8')
                if len(row)>6:
                    bureau  =row[6]#.decode('utf8')
                if len(row)>7:
                    tel     =row[7]#.decode('utf8')
            
                #1. Get profile picture (from the GDA server)
                pixfile=harpege[1:] + 'Harp0450855K.jpg'
                urlpix ='http://pocal.univ-orleans.fr/trombi/photos_etudiants/'
                urlpix+=harpege[:4] + '/' + pixfile
                #or else get them from the university's phone book
                urlpix2='https://annuaire.univ-orleans.fr/photo/index/c_uid/'+harpege+''
                #print(urlpix) ##for debugging only
                pixpath=outd + 'pictures/'+harpege+'.jpg'
                pixpath2=urlpix2
                if not os.path.isdir(outd+'pictures/'):
                    os.mkdir(outd+'pictures/')
                    ###os.chmod(outd+'pictures/', 0711) ### python2 only
                    os.chmod(outd+'pictures/', stat.S_IRWXU)
                #if not os.path.isfile(pixpath): ##no test on localpix (pdf trombi use these anyway) # no test at all cause we want to refresh them
                try:
                    src = requests.get(urlpix)
                    src.raise_for_status()
                    with open(pixpath, 'wb') as fd:
                        for chunk in src.iter_content(1024):
                            fd.write(chunk)
                    fd.close()
                    #we now use only png files
                    im = Image.open(pixpath)
                    im.save(''.join(pixpath.split('.')[:-1])+'.png')
                except (IOError,requests.exceptions.HTTPError):#, HTTPError):
                    atoutpix = False
                    #print('error in pix recovery ' + urlpix) #debug only
                    #print('error in pix recovery ' + pixpath) #debug only
                    #raise #debug only

                # 2. Prepare the html table for the output files
                bureaulink = ''
                if bureau != '' and mapi:
                    bureaulink = '<a href=\"#' + bureau + '\">' + bureau + '</a>'
                else:
                    bureaulink = bureau ##no link
                pixlink = ''
                if localpix and (atoutpix or os.path.isfile(pixpath)):
                    pixlink = ''.join(pixpath.split('.')[:-1])+'.png' ##link to the local pix (if available)
                    pixlink = 'static/' + '/'.join(pixlink.split('/')[-3:]) ## to remove the prefix FlaskApp/ (i.e. keep only the 2 last items of the URL)
                    #print(pixlink) ##debug only
                else:
                    pixlink = pixpath2 ##link to the university phone book
                    
                ## Main file:
                htmlfile.write('             <td><a href=\"#pers\" class=\"hasTooltip\">' + nom+'<span><img src=\"'+pixlink + '\" width=\"150px\"/></span></a></td>\n')
                htmlfile.write('             <td>' + prenom+'</td>\n')
                htmlfile.write('             <td>' + fonction+'</td>\n')
                htmlfile.write('             <td>' + domaine+'</td>\n')
                htmlfile.write('             <td><a href=\"mailto:' + email+'\">Contacter</a></td>\n')
                htmlfile.write('             <td>' + tel+'</td>\n')
                htmlfile.write('             <td>' + bureaulink+'</td>\n')
                htmlfile.write('             </tr>\n')
                
                htmlfile.write('             <tr>\n')

                ## Trombi file:
                if not atoutpix and not os.path.isfile(pixpath): ##that is, we cannot use local pix
                    pixpath = pixpath2                                 
                    localpixpath = outd + 'pictures/' + ''.join(pixpath.split('/')[-1])
                    ###src = urllib2.urlopen(pixpath) ### python2 only
                    #src = urllib.request.urlopen(pixpath)
                    src = requests.get(pixpath)
                    src.raise_for_status()                    
                    ##print(pixpath) #debug only
                    #dst = open(localpixpath+'.png','wb')
                    #shutil.copyfileobj(src, dst)
                    #dst.close()
                    with open(localpixpath+'.png', 'wb') as fd:
                        for chunk in src.iter_content(1024):
                            fd.write(chunk)
                    fd.close()
                    #BEWARE! PYFPDF DOES NOT SUPPORT INTERLACING SO IMAGE PROCESSING IS NEEDED:
                    img = Image.open(localpixpath+'.png')
                    size= list(img.size)
                    size[0] /= 2
                    size[1] /= 2
                    size[0] = int(size[0])
                    size[1] = int(size[1])
                    downsized=img.resize(size, Image.NEAREST)
                    downsized.save(localpixpath+'.png') ## and that rewriting of the png file is ok
                    to_del.append(localpixpath+'.png') #TO KEEP TRACK OF WHAT TO REMOVE ONCE THE TROMBI IS BUILT
                else:
                    im = Image.open(pixpath)
                    ImageFile.LOAD_TRUNCATED_IMAGES = True
                    localpixpath = ''.join(pixpath.split('.')[:-1]) ## i.e. pixpath without its extension
                    im.save(localpixpath+'.png') #here no keeping track is needed for pictures/ will be emptied anyway

                #ACTUAL TROMBI FILE WRITING:
                ##FPDF description:
                ybefore = mypdf.get_y()
                mypdf.image(localpixpath+'.png', x = 15 + (i%4)*(column_width+column_spacing), y = None, w = 25, h = 33, type = 'PNG', link = '')
                pres = ' \n' + nom + ' ' + prenom + '\n ' + fonction +'\n' + domaine + '\n' + tel +'\n'
                mypdf.multi_cell(column_width - 10, mypdf.font_size, pres, border=0, align = 'C', fill = False)
                #mypdf.cell(column_width, h = 10, txt = nom + ' ' + prenom, border = 0, ln = 0, align = '', fill = False, link = '')
                mypdf.set_xy((i%4+1)*(column_width + column_spacing) + mypdf.l_margin, ybefore)
                
                if i%4 == 3: ## 4 persons per line
                    mypdf.ln(65)
                    if il%4 == 3: ## 4 lines per page
                        mypdf.add_page()
                    il+=1
                # BEWARE: GLOBAL COUNTER! (not to be commented)
                i+=1

                # 3. Prepare variables for the js file (NB: MAP requires local pictures !)
                if bureau != '':
                    if bureau in imag.keys():
                        #imag[bureau].append(pixlink)
                        imag[bureau].append('static/' + '/'.join(localpixpath.split('/')[-3:]) + '.png')  ##WHENEVER POSSIBLE WE USE A LOCAL PICTURE
                        pers[bureau].append(nom + '\\n ' + prenom)
                    else:
                        #imag[bureau] = [pixlink]
                        imag[bureau] = ['static/' + '/'.join(localpixpath.split('/')[-3:]) + '.png']
                        pers[bureau] = [nom + '\\n ' + prenom]
            else:
                pass
    finally:
        file.close()

    # NOW, WE TRY TO GET PIX FROM POCAL FOR EACH PERSONNE ONE BY ONE, SO
    # THE MESSAGE BELOW BECOMES MEANINGLESS
    # if not atoutpix:
    #     print('Service de récupération des photos indisponible.')
        
    # Include the html template
    htmlfile.write('        </tr>\n')
    htmlfile.write('      </tbody>\n')
    htmlfile.write('    </table>\n')
    htmlfile.write(htmlEnd) ##to load the map
    htmlfile.close()

    # 4. Produce output js files
    if mapi:
        jsfile   = open(outd + '%s-map.js'%departement,'w', encoding="utf-8") ##this one will serve for the map
        persfile = open(outd + 'personnes.js','w', encoding="utf-8")
        imagfile = open(outd + 'images.js','w')
        persfile.write('var personnes = {}\n')
        imagfile.write('var images = {}\n')
        ## Process the imag and pers variables
        for buro in pers.keys():
            lineP = 'personnes[\"' + buro + '\"] = ['
            lineI = 'images[\"' + buro + '\"] = ['
            for ival in range(len(pers[buro])):
                lineP += '\"' + pers[buro][ival] + '\"'
                lineI += '\"' + imag[buro][ival] + '\"'
                if ival < (len(pers[buro]) - 1):
                    lineP += ', '
                    lineI += ', '
            lineP += ']\n'
            lineI += ']\n'
            persfile.write(lineP)
            imagfile.write(lineI)
        ## Close files
        persfile.close()
        imagfile.close()

        ## copier js files into map.js (beginning)
        jsfile.writelines([l for l in open(outd + 'personnes.js', 'r', encoding="utf-8").readlines()]) 
        jsfile.writelines([l for l in open(outd + 'images.js').readlines()]) 
        jsfile.writelines([l for l in open(outd + 'templateMap%s.js'%departement, encoding="utf-8").readlines()]) 
        jsfile.close()
        os.remove(outd + 'personnes.js')
        os.remove(outd + 'images.js')

    # 5. Produce PDF trombi
    if pdf:
        mypdf.output(outd + '%s-Trombi.pdf'%departement,'F')
        #To remove the locally downloaded pictures:
        if (not localpix) and os.path.isdir(outd+'pictures'):
            for f in to_del:
                #print(f)
                os.remove(f)

    # cleaning of temporary files
    if (not localpix) and os.path.isdir(outd+'pictures'):
        #print('cleaning going on') #debug only
        filelist = [ f for f in os.listdir(outd+'pictures') if f.startswith("p") ]
        for f in filelist:
            os.remove(outd+'pictures/'+f)
        os.rmdir(outd+'pictures/')

    res = False
    if os.path.isfile(outd + '%s.html'%departement):
        print('Liste des personnels générée.')
        res = True
    return res

if __name__ == "__main__":
    main(sys.argv[1:])
