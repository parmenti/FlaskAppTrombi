# -*- coding: utf-8 -*-
#! /usr/bin/python
#
# Flask Script to generate trombis
#
# Author: yannick.parmentier@univ-orleans.fr
# 2016/08/31

import os, time, datetime
from flask import Flask, render_template, request, send_from_directory
from werkzeug import secure_filename
##from flask.ext.cas import CAS
##from flask.ext.cas import login_required

app = Flask(__name__)
app.secret_key = 'super secrlksis!dn;et key'

from FlaskApp.generate_trombi import generate ## to be done after creating app

UPLOAD_FOLDER = '/tmp/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.debug = True
##cas = CAS(app)
app.config['CAS_SERVER'] = 'https://auth.univ-orleans.fr/cas' 
app.config['CAS_AFTER_LOGIN'] = 'main'

def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))

@app.route("/")
##@login_required
def main():
    return render_template('index.html')##, username = cas.username)

@app.route('/Administration')
def showAdministration():
    return app.send_static_file('Administration/Administration.html')

@app.route('/CHIMIE')
def showCHIMIE():
    return app.send_static_file('CHIMIE/CHIMIE.html')

@app.route('/GEA')
def showGEA():
    return app.send_static_file('GEA/GEA.html')

@app.route('/GMP')
def showGMP():
    return app.send_static_file('GMP/GMP.html')

@app.route('/GTE')
def showGTE():
    return app.send_static_file('GTE/GTE.html')

@app.route('/INFO')
def showINFO():
    return app.send_static_file('INFO/INFO.html')

@app.route('/QLIO')
def showQLIO():
    return app.send_static_file('QLIO/QLIO.html')

@app.route('/webtrombi')
def upload():
   return render_template('webtrombi.html')

@app.route('/uploader', methods = ['POST'])
def uploader():
   if request.method == 'POST':
      f = request.files['file']
      timestamp  = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S-')
      fichiercsv = (app.config['UPLOAD_FOLDER'] + timestamp + secure_filename(f.filename))
      #print(fichiercsv) #debug only
      f.save(fichiercsv)
      departement = request.form.get('dpt')
      #print(departement) #debug only
      res = None
      outdir = os.path.dirname(os.path.realpath(__file__)) + '/static/'
      if fichiercsv != '' and departement != '':
          zeoutdir = outdir + departement
          if not(os.path.isdir(zeoutdir)):
              os.mkdir(zeoutdir)
          #print(zeoutdir) #debug only
          try: ##recall that MAP requires local pictures
              if departement == 'INFO':
                  res = generate(fichiercsv, departement, 'Plan_IUT_RDC.jpg', True, True, zeoutdir) #StaffFile, Name, MapFile, Local, Pdf, Dir
              elif departement == 'Administration':
                  res = generate(fichiercsv, departement, 'plan-adm2.png', True, True, zeoutdir)
              elif departement == 'CHIMIE':
                  res = generate(fichiercsv, departement, '', True, True, zeoutdir)
              elif departement == 'GEA':
                  res = generate(fichiercsv, departement, '', False, True, zeoutdir)
              elif departement == 'GMP':
                  res = generate(fichiercsv, departement, '', False, True, zeoutdir)
              elif departement == 'GTE':
                  res = generate(fichiercsv, departement, '', False, True, zeoutdir)
              elif departement == 'QLIO':
                  res = generate(fichiercsv, departement, '', False, True, zeoutdir)
          except (RuntimeError,IndexError):
              res = False
      return render_template('index.html')

if __name__ == "__main__":
    app.run()

    
