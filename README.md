# FlaskAppTrombi

## Description

Application [Flask](http://flask.pocoo.org/) utilisée pour générer un listing de photos à partir d'une liste de comptes harpège (au format csv).

Pour lancer l'application, faire:
<pre>
python3 runserver.py
</pre>

puis ouvrir un navigateur à l'URL ``localhost:5000``

